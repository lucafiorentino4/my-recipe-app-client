import request from "./request";
function get(url) {
    return request({
        url: url,
        method: 'GET'
    });
}
// register user
function registerUser(user){
    return request({
        url: '/registerUser',
        method: 'POST',
        data: {
            user: user
        }
    })
}
// login user
function loginUser(user) {
    return request({
        url: '/loginUser',
        method: 'POST',
        data: {
            user: user
        }
    })
}
// like an recipe
function likeRecipe(user_id, recipe_id, like_recipe ) {
    return request({
        url: '/likeRecipe',
        method: 'POST',
        data: {
            recipe_id: recipe_id,
            like_recipe: like_recipe,
            user_id: user_id
        }
    })
}
// add new recipe
function addNewRecipe(newRecipe, addOccasion){

    let formData = new FormData();
    formData.append('recipeImage', newRecipe.selectedFile);
    formData.append('user_id', newRecipe.user_id);
    formData.append('name', newRecipe.name);
    formData.append('portions', newRecipe.portions);
    formData.append('portionUnit', newRecipe.portionUnit);
    formData.append('category', newRecipe.category);
    formData.append('recipetype', newRecipe.recipetype);
    formData.append('kitchenland', newRecipe.kitchenland);
    formData.append('preparationTime', newRecipe.preparationTime);
    formData.append('preparation_timeUnit_id', newRecipe.preparation_timeUnit_id);
    formData.append('cookingTime', newRecipe.cookingTime);
    formData.append('cooking_timeUnit_id', newRecipe.cooking_timeUnit_id);
    formData.append('waitingTime', newRecipe.waitingTime);
    formData.append('waiting_timeUnit_id', newRecipe.waiting_timeUnit_id);
    formData.append('description', newRecipe.description);
    formData.append('public', newRecipe.public);
    formData.append('difficulty_id', newRecipe.difficulty_id);
    formData.append('occasion', addOccasion.occasion);

    console.log(addOccasion);

    return request({
        url: '/addNewRecipe',
        method: 'POST',
        data: formData
    })
}
// update recipe
function updateRecipe(updateRecipe, recipe_id){
    let formData = new FormData();
    formData.append('recipe_id', recipe_id);
    formData.append('recipeImage', updateRecipe.selectedFile);
    formData.append('user_id', updateRecipe.user_id);
    formData.append('name', updateRecipe.name);
    formData.append('portions', updateRecipe.portions);
    formData.append('portionUnit', updateRecipe.portionUnit);
    formData.append('category', updateRecipe.category);
    formData.append('recipetype', updateRecipe.recipetype);
    formData.append('kitchenland', updateRecipe.kitchenland);
    formData.append('preparationTime', updateRecipe.preparationTime);
    formData.append('preparation_timeUnit_id', updateRecipe.preparation_timeUnit_id);
    formData.append('cookingTime', updateRecipe.cookingTime);
    formData.append('cooking_timeUnit_id', updateRecipe.cooking_timeUnit_id);
    formData.append('waitingTime', updateRecipe.waitingTime);
    formData.append('waiting_timeUnit_id', updateRecipe.waiting_timeUnit_id);
    formData.append('description', updateRecipe.description);
    formData.append('public', updateRecipe.public);
    formData.append('difficulty_id', updateRecipe.difficulty_id);
    formData.append('occasion', JSON.stringify(updateRecipe.occasionMap));

    return request({
        url: '/updateRecipe',
        method: 'POST',
        data: formData
    })
}
// get a recipe - login
function getRecipe(recipe_id, user_id) {
    return request({
        url: '/getRecipe',
        method: 'POST',
        data: {
            recipe_id: recipe_id,
            user_id: user_id
        }
    })
}
// get a recipe - offline
function getRecipeFromOffline(recipe_id) {
    return request({
        url: '/getRecipeFromOffline',
        method: 'POST',
        data: {
            recipe_id: recipe_id
        }
    })
}
// get all public recipes for user
function getAllPublicRecipes(user_id) {
    return request({
        url: '/getAllPublicRecipes',
        method: 'POST',
        data: {
            user_id: user_id
        }
    })
}
// get public recipes by search
function getAllPublicRecipesWithSearch(user_id, searchValue) {
    return request({
        url: '/getAllPublicRecipesWithSearch',
        method: 'POST',
        data: {
            user_id: user_id,
            searchValue: searchValue
        }
    })
}
// get 10 public recipes
function getTenPublicRecipes() {
    return request({
        url: '/getTenPublicRecipes',
        method: 'POST',
        data: {}
    })
}
// get all recipes of a user
function getAllRecipesForUser(user_id) {
    return request({
        url: '/getAllRecipesForUser',
        method: 'POST',
        data: {
            user_id: user_id
        }
    })
}
// get only 4 recipes
function getFirstFourRecipesForUser(user_id) {
    return request({
        url: '/getFirstFourRecipesForUser',
        method: 'POST',
        data: {
            user_id: user_id
        }
    })
}
// get all categories from db
function getDataFromDB() {
    return request({
        url: '/getDataFromDB',
        method: 'POST'
    })
}
// delete a recipe
function deleteRecipe(recipe_id) {
    return request({
        url: '/deleteRecipe',
        method: 'POST',
        data: {
            recipe_id: recipe_id
        }
    })
}

const apiService = {
    get, loginUser, registerUser,
    likeRecipe, addNewRecipe, updateRecipe, getDataFromDB, getFirstFourRecipesForUser,
    deleteRecipe, getAllRecipesForUser, getRecipe, getAllPublicRecipes,
    getTenPublicRecipes, getAllPublicRecipesWithSearch, getRecipeFromOffline
};

export default apiService;