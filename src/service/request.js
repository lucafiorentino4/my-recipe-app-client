import axios from 'axios';

import {config} from '../Config';

import {session} from '../helper/SessionStorage';

const client = axios.create({
    baseURL: config.apiServer.baseURL
});

const request = function (options) {

    const token = session.get('token');

    if (token) client.defaults.headers.common['authorization'] = 'Bearer ' + token;
    else client.defaults.headers.common['authorization'] = '';

    const onSuccess = function (response) {
        // TODO: add DEBUG switch to config
        console.debug('Request successful', response);

        return response.data;
    };

    const onError = function (error) {
        // TODO: add DEBUG switch to config
        console.error('Request failed', error.config);

        if (error.response) {
            console.error('Status:', error.response.status);
            console.error('Data:', error.response.data);
            console.error('Headers:', error.response.headers);
        } else {
            console.error('Error Message:', error.message);
        }

        return Promise.reject(error.response || error.message);

    };

    return client(options).then(onSuccess).catch(onError);

};

export default request;