
class Config {

    server = 'localhost';

    port = 5000;

    apiServer = {
        baseURL: 'http://' + this.server + ':' + this.port + '/api'
    };

    webServer = 'http://' + this.server + ':' + this.port + '/';

}

//singleton

export let config = new Config();