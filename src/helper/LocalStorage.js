class LocalStorage {

    set(key, value) {
        if (typeof value === 'object') value = JSON.stringify(value);
        localStorage.setItem(key, value);
    }

    get(key) {
        const value = localStorage.getItem(key);
        try {
            return JSON.parse(value);
        } catch (e) {
            return value;
        }
    }

    remove(key) {
        sessionStorage.removeItem(key);
    }

}

// singleton-pattern
export let storage = new LocalStorage();