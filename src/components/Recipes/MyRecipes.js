import React, {Component} from 'react';
import apiService from "../../service/apiService";
import MyRecipeList from "./MyRecipeComponents/MyRecipeList.js";
import ModalMessage from "../Globals/ModalMessage";
import auth from "../../service/auth";

class MyRecipes extends Component {

    constructor(props) {
        super(props);

        this.state = {
            myRecipes:  null,
            modalOpen: false,
            modalAction: null,
            recipe_item: null
        };

        this.getDataFromServer = this.getDataFromServer.bind(this);
        this.deleteRecipe = this.deleteRecipe.bind(this);
        this.modalConfirm = this.modalConfirm.bind(this);
        this.modalClose = this.modalClose.bind(this);

    }

    // delete recipe
    deleteRecipe(recipe_id) {
        let recipe_item = this.state.myRecipes.find(item => item.id === recipe_id);

        //show modal
        this.setState({
            modalOpen: true,
            modalAction: () => apiService.deleteRecipe(recipe_id).then(() => {
                this.getDataFromServer();
            }),
            recipe_item: recipe_item
        });
    }

    modalClose() {
        this.setState({modalOpen: false});
    }

    modalConfirm() {
        this.state.modalAction();
    }

    componentDidMount() {
        this.getDataFromServer();
    }

    // get data for recipes
    getDataFromServer(){
        let user_id = auth.user.id;

        apiService.getAllRecipesForUser(user_id).then(data => {
            this.setState({myRecipes: data, modalOpen: false, modalAction: null});
        });
    }

    render() {
        if (!this.state.myRecipes) return null;
        let recipe_name = (this.state.recipe_item) ? this.state.recipe_item.name : 'NAME';
        let recipe_id = (this.state.recipe_item) ? this.state.recipe_item.id : '0';

        return (
            <div>
                <MyRecipeList myRecipes={this.state.myRecipes} deleteRecipeHandler={this.deleteRecipe}/>
                <ModalMessage icon="trash"
                              header="DELETE"
                              message={`Do you really want to delete the recipe "${recipe_name}" (${recipe_id})?`}
                              modalOpen={this.state.modalOpen}
                              closeModalHandler={this.modalClose}
                              confirmModalHandler={this.modalConfirm}
                />
            </div>
        );
    }
}

export default MyRecipes;