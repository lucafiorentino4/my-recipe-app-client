import React, {Component} from 'react';
import {Icon, List, Rating} from "semantic-ui-react";
import {StyledLabel} from "../../Globals/StyledReactComponents/StyledReactComponents";
import auth from "../../../service/auth";

class RecipeInfoBlock extends Component {
    render() {
        let recipe = this.props.recipe;

        // Prepare occasion label
        let occasionLabels = this.props.recipe.occasion.map(item => {
            return <StyledLabel key={item.id} size="large" className="CIButtercup" horizontal>{ item.occasion_name }</StyledLabel>
        });

        return (
            <div>
                <label className={(recipe.like_recipe) ? "likeHeart redCI" : "likeHeart rosa"} >
                    <Icon name={(recipe.like_recipe) ? "heart" : "heart outline"} className="likeIcon" onClick={(auth.isAuthenticated) ? (() => {this.props.changeLikeHandler(recipe.id, !recipe.like_recipe)}) : null} />
                    <span className={(recipe.like_recipe) ? "likeCount redCI" : "likeCount rosa"}>{recipe.like_count}</span>
                    <p className="likeText">{(recipe.like_count > 1) ? "Persons" : "Person"} Like it</p>
                </label>
                <div className="RecipeInfosLabel">
                    <div className="RecipeInfosLabelContainer">
                        <label>Difficulty:</label>
                        <div className="RecipeInfoDiv">
                            <Rating disabled maxRating={5} rating={recipe.difficulty_id} icon='star' size='huge' />
                            <label>{(recipe.difficulty_text !== null) ? (" | " + recipe.difficulty_text) : "No difficulty added" }</label>
                        </div>
                    </div>
                    <div className="RecipeInfosLabelContainer">
                        <label>{recipe.portionUnit}:</label>
                        <div className="RecipeInfoDiv">
                            <Icon name='food' size='large'/> x {recipe.portions}
                        </div>
                    </div>
                </div>
                {/* if time is like 0 dont display */}
                <div className="recipeTime">
                    {(recipe.preparationTime === 0) ? " " : (
                        <label>
                            <span>Preparation Time:</span>
                            <span><Icon name='clock' />{this.props.calculateTimeToString(recipe.preparationTime)}</span>
                        </label>)}
                    {(recipe.cookingTime === 0) ? " " : (
                        <label>
                            <span>Cocking Time:</span>
                            <span><Icon name='clock' />{this.props.calculateTimeToString(recipe.cookingTime)}</span>
                        </label>)}
                    {(recipe.waitingTime === 0) ? " " : (
                        <label>
                            <span>Rest Time:</span>
                            <span><Icon name='clock' />{this.props.calculateTimeToString(recipe.waitingTime)}</span>
                        </label>)}
                </div>
                <List divided>
                    <List.Item>
                        <StyledLabel size="large" className="CIButtercup" horizontal>{ recipe.cat_name }</StyledLabel>
                        <StyledLabel size="large" className="CIButtercup" horizontal>{ recipe.rt_name }</StyledLabel>
                        <StyledLabel size="large" className="CIButtercup" horizontal>{ recipe.kl_name }</StyledLabel>
                        {occasionLabels}
                    </List.Item>
                </List>
            </div>
        );
    }
}

export default RecipeInfoBlock;