import React, {Component} from 'react';
import {Grid} from "semantic-ui-react";
import apiService from "../../../service/apiService";
import RecipeInfos from "./RecipeInfos";
import ModalMessage from "../../Globals/ModalMessage";
import {Redirect} from "react-router-dom";
import auth from "../../../service/auth";


class RecipeView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            recipeData: null,
            redirectToReferrer: false,
            modalOpen: false,
            modalAction: null,
            recipe_item: null,
        };

        this.getDataFromServer = this.getDataFromServer.bind(this);
        this.deleteRecipe = this.deleteRecipe.bind(this);
        this.modalConfirm = this.modalConfirm.bind(this);
        this.modalClose = this.modalClose.bind(this);
        this.changeLike = this.changeLike.bind(this);
    }
    modalClose() {
        this.setState({modalOpen: false});
    }
    modalConfirm() {
        this.state.modalAction();
    }

    componentDidMount() {
        this.getDataFromServer();
    }

    // get recipe
    getDataFromServer(){
        const { recipe_id } = this.props.match.params;
        let user_id= auth.user.id;

        apiService.getRecipe(recipe_id, user_id).then(data => {
            this.setState({recipeData: data});
        });
    }
    // delete recipe
    deleteRecipe(recipe_id) {
        // show modal
        let recipe_item = this.state.recipeData;

        this.setState({
            recipe_item: recipe_item,
            modalOpen: true,
            modalAction: () => apiService.deleteRecipe(recipe_id).then(() => {
                this.setState({redirectToReferrer: true});
            })
       });
    }
    // change like handler
    changeLike(recipe_id, like_recipe) {
        let user_id = auth.user.id;
        apiService.likeRecipe(user_id, recipe_id, like_recipe).then(() => {
            this.getDataFromServer();
        });
    }



    render() {

        let recipe = this.state.recipeData;
        const {from} = this.props.location.state || {from: {pathname: '/myrecipes'}};
        const {redirectToReferrer} = this.state;
        if (redirectToReferrer) return <Redirect to={from}/>;

        let recipe_name = (this.state.recipe_item) ? this.state.recipe_item.name : 'NAME';
        let recipe_id = (this.state.recipe_item) ? this.state.recipe_item.id : '0';

        return(
            <Grid className="extra paddings">
                <ModalMessage icon="trash"
                              header="DELETE"
                              message={`Do you really want to delete the recipe "${recipe_name}" (${recipe_id})?`}
                              modalOpen={this.state.modalOpen}
                              closeModalHandler={this.modalClose}
                              confirmModalHandler={this.modalConfirm}
                />
                <RecipeInfos recipe={recipe} changeLikeHandler={this.changeLike} deleteRecipeHandler={this.deleteRecipe}/>
            </Grid>
        )
    }
}
export default RecipeView;