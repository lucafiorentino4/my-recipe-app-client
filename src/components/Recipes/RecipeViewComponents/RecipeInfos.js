import React, {Component} from 'react';
import { Grid, Image, Button} from "semantic-ui-react";
import {config} from "../../../Config"
import {Link, withRouter} from "react-router-dom";

import RecipeInfoBlock from "./RecipeInfoBlock";
import Responsive from "semantic-ui-react/dist/commonjs/addons/Responsive";
//-------------------------------------

class RecipeInfos extends Component {

        constructor(props) {
            super(props);
            this.state = {
            }
        }
    //------------------------------------------------------------------------------------------------------------------
        // convert Time
        calculateTimeToString(time) {
            let hours = Math.trunc(time / 60);
            let minutes = (time % 60);
            return ((hours === 0) ? "" : hours + ':')  + minutes + ((hours > 0) ? " h." : " min.");
        }
    render() {
        // Get recipe Data
        if (!this.props.recipe) return null;
        let recipe = this.props.recipe;


        return (
            <Grid>
                <Grid.Row >
                    <Grid.Column>
                        <Button.Group labeled icon floated="left">
                            <Button onClick={this.props.history.goBack} className="lightGrey" icon='arrow alternate circle left' content='Back' />
                        </Button.Group>
                        <Button.Group labeled icon floated="right">
                            <Button as={Link} to={"/editrecipe/"+recipe.id} className="CIButtercup" icon='edit' content='Edit' />
                            <Button icon='trash' content='Delete' onClick={() => this.props.deleteRecipeHandler(recipe.id)}/>
                        </Button.Group>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column mobile={16} tablet={10} computer={10} >
                        <div className="RecipeViewPicContainerStyle">
                            <div className="RecipeViewLabelOnPicStyle">{recipe.name}</div>
                            <Image className="RecipeViewPicStyle" src={config.webServer + recipe.image_url} />
                        </div>
                    </Grid.Column>
                    <Grid.Column mobile={16} tablet={6} computer={6} >
                        <Responsive minWidth={Responsive.onlyTablet.minWidth}>
                            <RecipeInfoBlock recipe={recipe} calculateTimeToString={this.calculateTimeToString} changeLikeHandler={this.props.changeLikeHandler} />
                        </Responsive>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column mobile={16} tablet={16} computer={16} >
                        <Responsive {...Responsive.onlyMobile}>
                            <RecipeInfoBlock recipe={recipe} calculateTimeToString={this.calculateTimeToString} changeLikeHandler={this.props.changeLikeHandler} />
                        </Responsive>
                    </Grid.Column>
                    <Grid.Column className="extra paddings"  mobile={16} tablet={12} computer={12}>
                        <div className="RecipeInfosLabel">
                            <label>Recipe Description:</label>
                            {/*recipe.description */}
                        </div>
                        <div className="RecipeDescription">
                            <div dangerouslySetInnerHTML={{__html: recipe.description}} className="descriptionColor" />
                        </div>
                    </Grid.Column>
                </Grid.Row>
            </Grid>

        );
    }
}

export default withRouter(RecipeInfos);