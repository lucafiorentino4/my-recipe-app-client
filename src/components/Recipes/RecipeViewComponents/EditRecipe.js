import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import apiService from "../../../service/apiService";
import {Button, Form, Grid, Rating, Select, Checkbox, Message, Header, Image} from "semantic-ui-react";
import {config} from "../../../Config";
import auth from "../../../service/auth";
import MarkdownEditor from "../../Globals/MarkdownEditor";
import { EditorState } from "draft-js";
import {stateToHTML} from "draft-js-export-html";
import {stateFromHTML} from "draft-js-import-html";


const options = [
    {  key: 1, text: 'Minutes', value: 1 },
    {  key: 2, text: 'Hours', value: 2 }
];

const portionOptions = [
    { key: '1', text: 'Portion', value: 'Portion' },
    { key: '2', text: 'Piece', value: 'Piece' }
];


class EditRecipe extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToPage: false,

            editorState: EditorState.createWithContent(stateFromHTML("<b>BOLD</b>")),

            /* Recipe Data */
            errors: {
                name: null,
                portions: null,
                portionUnit: null,
                description: null,
                public: false,
                difficulty_id: null,
                selectedFile: null,
            }
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.getDataFromServer = this.getDataFromServer.bind(this);
        this.occasionChangeHandler = this.occasionChangeHandler.bind(this);
        this.submit = this.submit.bind(this);
        this.uploadHandler = this.uploadHandler.bind(this);
        this.checkForErrors = this.checkForErrors.bind(this);
        this.changeEditorHandler = this.changeEditorHandler.bind(this);
    }


    /* other functions */
    componentDidMount() {
        this.getDataFromServer();
    }

    getDataFromServer(){
        apiService.getDataFromDB().then(data => {
            let allCategoriesData = data.allCategoriesData;
            let allRecipeTypes = data.allRecipeTypes;
            let allKitchenLands = data.allKitchenLands;
            let allOccasions = data.allOccasions;
            this.setState({
                allCategories: allCategoriesData,
                allRecipeType: allRecipeTypes,
                allKitchenLand: allKitchenLands,
                allOccasion: allOccasions});
        });

        const { recipe_id } = this.props.match.params;
        let user_id = auth.user.id;

        apiService.getRecipe(recipe_id, user_id).then(res => {
            let editorState = EditorState.createWithContent(stateFromHTML(res.description));
            this.setState({recipeData: res, editorState: editorState});
        });
    }

    /* Handlers */
    changeHandler(evt, {name, value, checked, rating}) {
        let values = this.state.recipeData;
        values[name] = value;
        if (typeof checked !== 'undefined') values[name] = checked;
        if (typeof rating !== 'undefined') values[name] = rating;

        this.setState({recipeData: values});
    }

    occasionChangeHandler(evt, {name, value}) {
       let addOccasion = this.state.recipeData;
        addOccasion['occasionMap'] = value;
        this.setState({recipeData: addOccasion});
    }

    changeEditorHandler(editorState){
        this.setState( {editorState: editorState})
    }


    uploadHandler(evt, {name, value}){
        let values = this.state.recipeData;
        values['selectedFile'] = evt.target.files[0];
        values[name] = value;
        this.setState({recipeData: values});
    }

    checkForErrors() {
        // variables
        let values = this.state.recipeData;
        let errors = this.state.errors;
        // set the error on default false
        let isError = false;

        // check for empty fields
        for (let [key, value] of Object.entries(values)) {
            if (value === '') {
                // if empty field, than set error on true
                isError = true;
                errors[key] = key.charAt(0).toUpperCase() + key.substring(1) + ` is empty.`;
                if(errors.portionUnit) errors.portionUnit = 'Portion unit is empty';
            } else {
                errors[key] = null
            }
        }
        // return the error
        return (isError) ? errors : false;
    }

    submit(){
        let updateRecipe = this.state.recipeData;
        let recipe_id = this.state.recipeData.id;
        let errors = this.checkForErrors();

        if(!errors) {
            updateRecipe.description = stateToHTML(this.state.editorState.getCurrentContent());
            apiService.updateRecipe(updateRecipe, recipe_id).then(() => {
               this.setState({redirectToPage: true});
                console.log('submit');
            });
        } else {
            this.setState({errors:errors});
        }
    }

    render() {
        if (!this.state.recipeData || !this.state.allCategories ||
            !this.state.allRecipeType || !this.state.allKitchenLand || !this.state.allOccasion) return null;

        let standardTimeUnit = 1; /* minutes */
        // eslint-disable-next-line  no-lone-blocks
        {/* RENDER CATEGORIES */}
        let categoryOptions = this.state.allCategories.map(item => {
            return {key:item.id, text:item.name, value: item.id};
        });
        let recipeTypeOptions = this.state.allRecipeType.map(item => {
            return {key:item.id, text:item.name, value: item.id};
        });
        let kitchenlandOptions = this.state.allKitchenLand.map(item => {
            return {key:item.id, text:item.name, flag: item.countryCode, value:item.id, name: item.name};
        });
        let occasionsOptions = this.state.allOccasion.map(item => {
            return {key:item.id, text:item.name, value: item.id};
        });

        const {redirectToPage} = this.state;

        let recipe = (!this.state.recipeData) ? null : this.state.recipeData;

        if (redirectToPage) return <Redirect to={"/recipeview/"+ recipe.id}/>;

        const {errors} = this.state;
        let errList = [];
        // eslint-disable-next-line  no-unused-vars
        for (let [key, value] of Object.entries(errors)) {
            if (value) errList.push(value);
        }

        let errorList = !!errors.name || !!errors.portions || !!errors.portionUnit ||
            !!errors.description || !!errors.selectedFile ;



        return (
            <Grid className="marginTopDown">
                <Button.Group labeled icon floated="left" className="button margin">
                    <Button onClick={this.props.history.goBack} className="lightGrey" icon='arrow alternate circle left' content='Back' />
                </Button.Group>
                <Grid.Row centered>
                    <Grid.Column mobile={16} tablet={16} computer={12}>
                        <Form error={(errorList)} >
                            <Grid>
                                <Grid.Row >
                                    <Grid.Column mobile={16} tablet={12} computer={16}>
                                        <Header>Edit recipe</Header>
                                        <Message error header='Something went wrong!' list={errList} />
                                    </Grid.Column>
                                </Grid.Row>
                                <Grid.Row >
                                    <Grid.Column mobile={16} tablet={8} computer={6} className="paddings">
                                        <Image className="RecipeViewPicStyle paddings medium" src={config.webServer + recipe.image_url} />
                                    </Grid.Column>

                                    <Grid.Column mobile={16} tablet={8} computer={10} className="paddings">
                                        {/* Name */}
                                        <Form.Input label='Recipe name' placeholder='Ex: Carbonara' name="name" onChange={this.changeHandler}  error={(!!errors.name) ? { content: errors.name } : null}  value={recipe.name}/>
                                        {/* Image Upload */}
                                        <Form.Input label="Aktualize picture" type='file' onChange={this.uploadHandler}/>
                                        {/* Difficulty */}
                                        <Form.Group grouped className="paddings">
                                            <label className="labelRating" >Difficulty:</label>
                                            <Rating maxRating={5} icon='star' size='huge' name="difficulty_id" onRate={this.changeHandler} rating={recipe.difficulty_id}/>
                                            <label className="labelRating" >{recipe.difficulty_text}</label>
                                        </Form.Group>
                                    </Grid.Column>
                                </Grid.Row>
                                <Grid.Row>
                                    {/* Quantity */}
                                    <Grid.Column mobile={4} tablet={3} computer={3} className="paddings">
                                            <Form.Input fluid type="number" label='Quantity' placeholder='Ex: 2' name='portions'  onChange={this.changeHandler} error={(!!errors.portions) ? { content: errors.portions } : null}  value={recipe.portions}/>
                                    </Grid.Column>
                                    {/* Quantity Unit */}
                                    <Grid.Column mobile={12} tablet={5} computer={5} className="paddings">
                                        <Form.Input fluid control={Select} label='Portions' placeholder='Ex: Portions' name='portionUnit' options={portionOptions} onChange={this.changeHandler}  error={(!!errors.portionUnit) ? { content: errors.portionUnit } : null}  value={recipe.portionUnit}/>
                                    </Grid.Column>
                                    {/* Categories */}
                                    <Grid.Column mobile={8} tablet={4} computer={4} className="paddings">
                                        <Form.Input fluid control={Select} label='Categories' placeholder='Ex: Vegan vs Not Veg..' onChange={this.changeHandler} name='category' options={categoryOptions} value={recipe.category} />
                                    </Grid.Column>
                                    {/* Recipe type */}
                                    <Grid.Column mobile={8} tablet={4} computer={4} className="paddings">
                                        <Form.Input fluid control={Select} label='Recipe Type' placeholder='Ex: Dessert' name='recipetype' options={recipeTypeOptions} onChange={this.changeHandler} value={recipe.recipetype} />
                                    </Grid.Column>

                                    {/* Kitchen */}
                                    <Grid.Column mobile={8} tablet={3} computer={3} className="paddings">
                                        <Form.Input fluid control={Select} label='Kitchen' placeholder='Ex: Italian kitchen' name='kitchenland' options={kitchenlandOptions} onChange={this.changeHandler} value={recipe.kitchenland} />
                                    </Grid.Column>
                                    {/* Occasion */}
                                    <Grid.Column mobile={16} tablet={13} computer={13} className="paddings">
                                        <Form.Input  control={Select} label='Occasion' placeholder='Ex: Ester' name='occasion' options={occasionsOptions} multiple selection onChange={this.occasionChangeHandler} value={(!recipe.occasionMap) ? ([]) : recipe.occasionMap} />
                                    </Grid.Column>
                                    {/* Preparation Time */}
                                    <Grid.Column mobile={6} tablet={4} computer={4} className="paddings">
                                        <Form.Input fluid type="number" label='Preparation time' placeholder='Preparation time..' name="preparationTime" onChange={this.changeHandler} value={recipe.preparationTime} />
                                    </Grid.Column>
                                    {/* Preparation Time Unit */}
                                    <Grid.Column mobile={10} tablet={4} computer={4} className="paddings">
                                        <Form.Input fluid control={Select} label='Duration' options={options} placeholder='Time unit' name='preparation_timeUnit_id' onChange={this.changeHandler} value={standardTimeUnit} />
                                    </Grid.Column>
                                    {/* Cooking Time */}
                                    <Grid.Column mobile={6} tablet={4} computer={4} className="paddings">
                                        <Form.Input type="number" label='Cooking time' placeholder='Cooking time..' name="cookingTime" onChange={this.changeHandler} value={recipe.cookingTime} />
                                    </Grid.Column>
                                    {/* Cooking Time  Unit */}
                                    <Grid.Column mobile={10} tablet={4} computer={4} className="paddings">
                                        <Form.Input  fluid control={Select} label='Duration' options={options} placeholder='Time unit' name='cooking_timeUnit_id' onChange={this.changeHandler} value={standardTimeUnit} />
                                    </Grid.Column>
                                    {/* Waiting Time */}
                                    <Grid.Column mobile={6} tablet={4} computer={4} className="paddings">
                                        <Form.Input type="number" label='Waiting time' placeholder='Wartezeit..' name="waitingTime"  onChange={this.changeHandler} value={recipe.waitingTime} />
                                    </Grid.Column>
                                    {/* PWaiting Time Unit */}
                                    <Grid.Column mobile={10} tablet={4} computer={4}  className="paddings">
                                        <Form.Input fluid control={Select} label='Duration' options={options} placeholder='Time unit' name='waiting_timeUnit_id' onChange={this.changeHandler} value={standardTimeUnit} />
                                    </Grid.Column>
                                        {/* Description */}
                                    <Grid.Column mobile={16} tablet={16} computer={16} className="paddings">
                                        <MarkdownEditor changeEditorHandler={this.changeEditorHandler} editorState={this.state.editorState}/>
                                    </Grid.Column>
                                    {/* private public */}
                                    <Grid.Column mobile={16} tablet={16} computer={16} className="paddings">
                                            <Checkbox name="public" checked={!!recipe.public} onChange={this.changeHandler} toggle />
                                            <label className="checkBoxLabel" >Your recipe is now set on: <span className="doTextBold"> "{(recipe.public) ? 'Public' : 'Private'}" </span> </label>
                                    </Grid.Column>
                                    <Grid.Column className="paddings">
                                        <Button className="CIButtercup" onClick={this.submit}>Submit</Button>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Form>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}
export default EditRecipe;