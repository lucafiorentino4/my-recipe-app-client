import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import apiService from "../../service/apiService";
import {Button, Form, Grid, Rating, Select, Checkbox, Message} from "semantic-ui-react";
import auth from "../../service/auth";
import MarkdownEditor from "../Globals/MarkdownEditor";

import { EditorState } from "draft-js";
import {stateFromHTML} from "draft-js-import-html";
import {stateToHTML} from "draft-js-export-html";

// prepare options for inputs
const timeUnitOptions = [
    { key: '1', text: 'Minutes', value: '1' },
    { key: '2', text: 'Hours', value: '2' }
];
const portionOptions = [
    { key: '1', text: 'Portion', value: 'Portion' },
    { key: '2', text: 'Piece', value: 'Piece' }
];

class NewRecipe extends Component {

    constructor(props) {
        super(props);
        let user_id = auth.user.id;
        this.state = {
            redirectToHome: false,
            allCategories: [],
            allRecipeType: [],
            allKitchenLand: [],
            allOccasion: [],
            editorState: EditorState.createWithContent(stateFromHTML('')),

            /* Recipe Data */
            values: {
                user_id: user_id,
                name: "",
                portions: "",
                portionUnit: "",
                category: "",
                recipetype: "",
                kitchenland: "",
                preparationTime: 0,
                preparation_timeUnit_id: 1,
                cookingTime: 0,
                cooking_timeUnit_id:1,
                waitingTime: 0,
                waiting_timeUnit_id:1,
                description: "",
                public: true,
                difficulty_id: 0,
                selectedFile: '',
            },
            errors: {
                name: null,
                portions: null,
                portionUnit: null,
                public: false,
                difficulty_id: null,
                selectedFile: null,
            },
            editor: '',
            addOccasion: {occasion: ""}
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.getDataFromServer = this.getDataFromServer.bind(this);
        this.occasionChangeHandler = this.occasionChangeHandler.bind(this);
        this.submit = this.submit.bind(this);
        this.uploadHandler = this.uploadHandler.bind(this);
        this.checkForErrors = this.checkForErrors.bind(this);
        this.editorHandler = this.editorHandler.bind(this);
        this.changeEditorHandler = this.changeEditorHandler.bind(this);
    }

    /* other functions */
    componentDidMount() {
        this.getDataFromServer();
    }

    // get data from Server
    getDataFromServer(){
        // get all categories Data
        apiService.getDataFromDB().then(data => {
            let allCategoriesData = data.allCategoriesData;
            let allRecipeTypes = data.allRecipeTypes;
            let allKitchenLands = data.allKitchenLands;
            let allOccasions = data.allOccasions;
            this.setState({
                allCategories: allCategoriesData,
                allRecipeType: allRecipeTypes,
                allKitchenLand: allKitchenLands,
                allOccasion: allOccasions});
        });
    }

    // validate the inputs and check for errors
    checkForErrors() {
        // variables
        let values = this.state.values;
        let errors = this.state.errors;
        // set the error on default false
        let isError = false;

        // check for empty fields
        for (let [key, value] of Object.entries(values)) {
            if (value === '') {

              // if empty field, than set error on true
                isError = true;
                errors[key] = key.charAt(0).toUpperCase() + key.substring(1) + ` is empty.`;
                if(errors.portionUnit) errors.portionUnit = 'Portion unit is empty';
                if(errors.description) errors.description = null;
            }
            else {
                errors[key] = null
            }
        }
        // return the error
        return (isError) ? errors : false;
    }

    /* Handlers */
    // set state for files
    uploadHandler(evt, {name, value}){
        let values = this.state.values;
        values['selectedFile'] = evt.target.files[0];
        values[name] = value;
        this.setState({values: values});
    }
    // set state for inputs
    changeHandler(evt, {name, value, checked, rating}) {
        let values = this.state.values;
        values[name] = value;
        if (typeof checked !== 'undefined') values[name] = checked;
        if (typeof rating !== 'undefined') values[name] = rating;

        this.setState({values: values});
    }

    editorHandler(editorState){
        this.setState({editorState:editorState});
    }

    // set state for occasion
    occasionChangeHandler(evt, {name, value}) {
        let addOccasion = this.state.addOccasion;
        addOccasion[name] = value;
        this.setState({addOccasion: addOccasion});
    }


    changeEditorHandler(editorState){
        this.setState( {editorState: editorState})
    }


    // get the states and send to the server
    submit(){
        let newRecipe = this.state.values;
        newRecipe.description = stateToHTML(this.state.editorState.getCurrentContent());
        // variables
        let addOccasion = this.state.addOccasion;
        let errors = this.checkForErrors();
        console.log(errors);
        // check for errors, if there are no errors add new recipe
        if(!errors) {
           apiService.addNewRecipe(newRecipe, addOccasion).then(() => {
               this.setState({redirectToHome: true});
           });
        } else {
            this.setState({errors:errors});
        }
    }


    render() {
        // eslint-disable-next-line  no-lone-blocks
        {/* RENDER CATEGORIES OPTIONS */}
        let categoryOptions = this.state.allCategories.map(item => {
            return {key:item.id, text:item.name, value:item.id};
        });
        let recipeTypeOptions = this.state.allRecipeType.map(item => {
            return {key:item.id, text:item.name, value:item.id};
        });
        let kitchenlandOptions = this.state.allKitchenLand.map(item => {
            return {key:item.id, text:item.name, flag:item.countryCode, value:item.id, name: item.name};
        });
        let occasionsOptions = this.state.allOccasion.map(item => {
            return {key:item.id, text:item.name, value:item.id};
        });

        const {values} = this.state;
        const {redirectToHome} = this.state;

        if (redirectToHome) return <Redirect to={"/"}/>;

        const {errors} = this.state;
        let errList = [];
        // eslint-disable-next-line  no-unused-vars
        for (let [key, value] of Object.entries(errors)) {
            if (value) errList.push(value);
        }

        let errorList = !!errors.name || !!errors.portions || !!errors.portionUnit || !!errors.selectedFile ;


        return (
            <Grid stackable centered columns={16} className="marginTopDown">
                <Grid.Column mobile={16} tablet={12} computer={12}>
                   <Form error={(errorList)}>
                       <Message error header='Something went wrong!' list={errList} />
                       {/* Image Upload */}
                       <Form.Group widths='equal'>
                           <Form.Input type='file' onChange={this.uploadHandler} error={(!!errors.selectedFile) ? { content: errors.selectedFile } : null} />
                       </Form.Group>
                        {/* Name */}
                        <Form.Group className="paddings">
                            <Form.Input width={10} label='Recipe name' placeholder='Ex: Carbonara' name="name"
                                        onChange={this.changeHandler} error={(!!errors.name) ? { content: errors.name } : null} />
                        </Form.Group>
                        {/* Difficulty */}
                        <Form.Group grouped className="paddings">
                            <label className="labelRating" >Difficulty:</label>
                            <Rating maxRating={5} icon='star' size='huge' name="difficulty_id" onRate={this.changeHandler}  error={(!!errors.difficulty_id) ? { content: errors.difficulty_id } : null}/>
                        </Form.Group>
                        {/* Quantity */}
                        <Form.Group className="paddings" >
                            <Form.Input type="number" label='Quantity' placeholder='Ex: 2' name='portions'
                                        value={values.portions} onChange={this.changeHandler} error={(!!errors.portions) ? { content: errors.portions } : null} />
                            <Form.Input  fluid control={Select} label='Portions' placeholder='Ex: Portions' name='portionUnit'
                                         options={portionOptions} onChange={this.changeHandler} error={(!!errors.portionUnit) ? { content: errors.portionUnit } : null} />
                        </Form.Group>
                        {/* Categories */}
                        <Form.Group >
                            <Form.Input  control={Select} label='Categories' placeholder='Ex: Vegan vs Not Veg..' onChange={this.changeHandler}
                                         name='category' options={categoryOptions} error={(!!errors.category) ? { content: errors.category } : null} />
                            <Form.Input  control={Select} label='Recipe Type' placeholder='Ex: Dessert' name='recipetype'
                                         options={recipeTypeOptions} onChange={this.changeHandler} error={(!!errors.recipetype) ? { content: errors.recipetype } : null} />
                        </Form.Group>
                        {/* Kitchen */}
                        <Form.Group className="paddings">
                            <Form.Input  control={Select} label='Kitchen' placeholder='Ex: Italian kitchen' name='kitchenland'
                                         options={kitchenlandOptions} onChange={this.changeHandler} error={(!!errors.kitchenland) ? { content: errors.kitchenland } : null} />
                            <Form.Input  control={Select} label='Occasion' placeholder='Ex: Ester' name='occasion'
                                         options={occasionsOptions} multiple selection onChange={this.occasionChangeHandler} error={(!!errors.occasion) ? { content: errors.occasion } : null} />
                        </Form.Group>
                        {/* Preparation Time */}
                        <Form.Group className="paddings">
                            <Form.Input type="number" label='Preparation time' placeholder='Preparation time..' name="preparationTime"
                                        value={values.preparationTime} onChange={this.changeHandler} />
                            <Form.Input fluid control={Select} label='Duration' options={timeUnitOptions} placeholder='Time unit' name='preparation_timeUnit_id' onChange={this.changeHandler}/>
                        </Form.Group>
                        {/* Cooking Time */}
                        <Form.Group  className="paddings">
                            <Form.Input type="number" label='Cooking time' placeholder='Cooking time..' name="cookingTime"
                                        value={values.cookingTime} onChange={this.changeHandler}/>
                            <Form.Input  fluid control={Select} label='Duration' options={timeUnitOptions} placeholder='Time unit' name='cooking_timeUnit_id' onChange={this.changeHandler}/>
                        </Form.Group>
                        {/* Waiting Time */}
                        <Form.Group  className="paddings">
                            <Form.Input type="number" label='Waiting time' placeholder='Wartezeit..' name="waitingTime"
                                        value={values.waitingTime} onChange={this.changeHandler}/>
                            <Form.Input fluid control={Select} label='Duration' options={timeUnitOptions} placeholder='Time unit' name='waiting_timeUnit_id' onChange={this.changeHandler}/>
                        </Form.Group>
                       {/* Description */}
                       <MarkdownEditor changeEditorHandler={this.changeEditorHandler} editorState={this.state.editorState}/>
                        {/* privatePublic */}
                        <Form.Group className="paddings">
                            <Checkbox name="public" checked={values.public} onChange={this.changeHandler} toggle />
                            <label className="checkBoxLabel" >Your recipe is now set on: <span className="doTextBold"> "{(values.public) ? 'Public' : 'Private'}" </span> </label>
                        </Form.Group>
                        <Button  className="CIButtercup" onClick={this.submit}>Submit</Button>
                    </Form>
                </Grid.Column>
            </Grid>

        );
    }
}

export default NewRecipe;