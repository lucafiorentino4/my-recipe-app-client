import React, {Component} from 'react';
import { Grid } from 'semantic-ui-react';
import MyRecipeItem from "./MyRecipeItem.js";

class MyRecipeList extends Component {

    render() {
       let recipeItem = this.props.myRecipes.map((item) => {
            return  <MyRecipeItem key={item.id} myRecipeItem={item} deleteRecipeHandler={this.props.deleteRecipeHandler}/>;
        });

        return(
                <Grid className="marginTopDown">
                        {recipeItem}
                </Grid>
        );
    }
}

export default MyRecipeList;