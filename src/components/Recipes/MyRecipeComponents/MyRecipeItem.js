import React, {Component} from 'react';
import {Card, Icon, Rating, Grid, Button} from "semantic-ui-react";
import {Link} from 'react-router-dom';
import {config} from "../../../Config";
import {StyledListCardImage, StyledCardHeader} from "../../Globals/StyledReactComponents/StyledReactComponents.js"
import Image from "semantic-ui-react/dist/commonjs/elements/Image";
//-------------------------------------

class  MyRecipeItem extends Component {

    portionHandler(portion, portionUnt){
        if(portion >= 1) {
            if(portionUnt === "Piece"){
                return "Pieces";
            } if (portionUnt === "Portion") {
                return "Portions"; }
        } else {
           return portionUnt;
        }
    }

    // convert time
    calculateTimeToString(time) {
        let hours = Math.trunc(time / 60);
        let minutes = (time % 60);
        return ((hours === 0) ? "" : hours + ':')  + minutes + ((hours > 0) ? " h." : " min.");
    }

    render() {
        let recipe = this.props.myRecipeItem;

        let addedTimes = recipe.preparationTime + recipe.waitingTime + recipe.cookingTime;

        return (
            <Grid.Column mobile={8} tablet={4} computer={4} >
                 <Card centered>
                     <StyledListCardImage wrapped ui={false} >
                         <Image href={"/recipeview/" + recipe.id} src={config.webServer + recipe.image_url} alt={recipe.name}/>
                         <label className="timeStyleCard"><Icon name='clock' />{this.calculateTimeToString(addedTimes)}</label>
                     </StyledListCardImage>
                    <Card.Content as={Link} to={"/recipeview/" + recipe.id}>
                        <Card.Header>
                            <label className="likeHeart redCI" >
                                <Icon name="heart" className="likeIcon"/>
                                <span className="likeCount">{recipe.like_count}</span>
                            </label>
                        </Card.Header>
                        <StyledCardHeader>{recipe.name}</StyledCardHeader>
                        <Card.Meta>
                            <Rating disabled maxRating={5} rating={recipe.difficulty_id} icon='star' size='large' />
                            <span className='date'>{recipe.difficulty_text}</span>
                        </Card.Meta>
                        <Card.Meta>
                            <span>For: {recipe.portions} {(recipe.portionUnit === null) ? "Persons" : this.portionHandler(recipe.portions, recipe.portionUnit)}</span>
                        </Card.Meta>
                    </Card.Content>
                     <Button.Group>
                         <Button as={Link} to={"/recipeview/"+recipe.id} className="CIButtercup">Open</Button>
                         <Button.Or />
                         <Button onClick={() => this.props.deleteRecipeHandler(recipe.id)} ><i className="icon trash"/></Button>
                     </Button.Group>
                </Card>
            </Grid.Column>
        );
    }
}

export default MyRecipeItem;