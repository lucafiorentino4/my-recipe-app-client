import React, {Component} from 'react';


import { Link, withRouter } from "react-router-dom";

import {Icon, Menu} from 'semantic-ui-react'


class NavbarMobile extends Component {
    render() {
        let pathname = this.props.location.pathname;
        
        return (
                <Menu.Menu className="navbarMobile CIWhite">
                    <Menu.Item as={Link} to="/myrecipes" name='MyRecipes'>
                        <span className="iconStyle" >
                            <i className={"fas fa-concierge-bell"}/>
                            <p>My Recipes</p>
                        </span>

                    </Menu.Item>

                    <Menu.Item  className="navMenuPencilMobile circularButtonMobile CIButtercup" as={Link} to="/newrecipe" name='NewRecipe' >
                        <span className="iconStyle" >
                            <i className={(pathname !== '/newrecipe') ? "icon pencil" : "icon plus"} />
                        </span>
                    </Menu.Item>

                    <Menu.Item as={Link} to="/explorerecipes" name='ExploreRecipes'>
                        <span className="iconStyle">
                            <Icon className='search'/>
                            <p>Explore</p>
                        </span>
                    </Menu.Item>
                </Menu.Menu>
        );
    }
}

export default withRouter(NavbarMobile);