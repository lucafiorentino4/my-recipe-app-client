import React, {Component} from "react";

class Logo extends Component {
    render() {
        return (
            <img src={require("../../assets/Logo.svg")} alt="MyRecipe" title="My Recipe, Cook it easy" />
        );
    }
}

export default Logo;


