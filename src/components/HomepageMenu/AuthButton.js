import React, {Component} from 'react';

import { Link, withRouter } from "react-router-dom";

import {Icon, Menu} from 'semantic-ui-react';

import auth from '../../service/auth';

class AuthButton extends Component {
    render() {

        let sidebar = this.props.sidebar;
        let mobile = this.props.mobile;
        let pathname = this.props.location.pathname;
        let history = this.props.history;

        return auth.isAuthenticated ? (
            (!mobile)
            ? (<Menu.Item style={ {display: sidebar ? 'block' : 'flex', marginRight: mobile ? '0rem' : '6rem' }} as="a" position='right' onClick={() => {
                    auth.signout(() => history.push('/'));}}>
                    Logout
                </Menu.Item>
            ) : (
                <Menu.Item style={ {display: sidebar ? 'block' : 'flex', marginRight: mobile ? '0rem' : '6rem' }} as="a"  onClick={() => {
                    auth.signout(() => history.push('/'));}}>
                    { (!sidebar)
                    ? ( <span className="iconStyleNavTop">
                            <Icon className='user' />
                            <p>Logout</p>
                        </span>
                    ) : (
                        "Logout"
                    )}
                </Menu.Item>
            )
        ) : (
            (!mobile)
            ? ( <Menu.Menu style={{display: sidebar ? 'block' : 'flex', marginRight: mobile ? '0rem' : '6rem'}} position="right">
                    <Menu.Item as={Link} to="/login" active={pathname === '/login'} position='right'>Login</Menu.Item>
                    <Menu.Item as={Link} to="/register" active={pathname === '/register'} >Register</Menu.Item>
                </Menu.Menu>
            ) : (
               <div style={{display: sidebar ? 'block' : 'flex', marginRight: mobile ? '0rem' : '6rem'}} >
                  { (!sidebar)
                   ? (<Menu.Item as={Link} to="/login" active={pathname === '/login'} position='right'>
                           <span className="iconStyleNavTop">
                             <Icon className='user outline'/>
                             <p>Login</p>
                           </span>
                       </Menu.Item>
                   ) : (
                       <Menu.Item as={Link} to="/register" active={pathname === '/register'}>
                          Register
                       </Menu.Item>
                   )}
               </div>
            )
        );
    }
}

export default withRouter(AuthButton);