import React, {Component} from 'react';

import { Link, withRouter } from "react-router-dom";

import {Menu, Container, Divider, Button} from 'semantic-ui-react';

import AuthButton from './AuthButton';
import Logo from "./Logo";

class HomepageMenu extends Component {

    render() {

        let sidebar = this.props.sidebar;
        let mobile = this.props.mobile;
        let pathname = this.props.location.pathname;

        return (
                <Menu className="CIWhite" secondary pointing vertical={mobile} style={{float: sidebar ? 'left':'none', backgroundColor: 'white', marginBottom:'0'}} >
                    <Container style={{display:sidebar ? 'block' : 'flex', position: "relative"}} >

                        <Menu.Item className="Logo" as={Link} to="/" name='home' ><Logo /></Menu.Item>
                        <Menu.Item as={Link} to="/" name='home' >Home</Menu.Item>
                        {(!mobile)
                            ?  <Menu.Item as={Link} to="/myrecipes" name='MyRecipes' active={pathname === '/myrecipes'} >My Recipes</Menu.Item>
                            :  <Menu.Item as={Link} to="/myrecipes" name='MyRecipes' active={pathname === '/myrecipes' } style={{display:'flex', justifyContent: "space-between"}} >My Recipes<i className={"fas fa-concierge-bell"}/></Menu.Item>}
                        {(!mobile)
                            ?  <Button className="circularButton CIButtercup" circular icon={(pathname !== '/newrecipe') ? 'pencil' : 'plus'} as={Link} to="/newrecipe" name='NewRecipe' active={pathname === '/newrecipe'} />
                            :  <Menu.Item as={Link} to="/newrecipe" name='NewRecipe' active={pathname === '/newrecipe'} ><i className={(pathname !== '/newrecipe') ? "icon pencil" : "icon plus"} /> Add new recipe</Menu.Item>}
                        {(mobile)
                            ?  <Menu.Item as={Link} to="/explorerecipes" name='SearchRecipe' active={pathname === '/explorerecipes'} ><i className="icon search" /> Explore Recipes</Menu.Item>
                            :  ' '}
                        <Divider />
                        {(!mobile)
                            ?  <Menu.Item floated="right" as={Link} to="/explorerecipes" name='ExploreRecipes' active={pathname === '/newrecipe'} ><i className="icon search" /> Explore Recipes</Menu.Item>
                            :  ' '}
                       <AuthButton mobile={mobile} sidebar={sidebar}/>


                    </Container>
                </Menu>
        );
    }
}

export default withRouter(HomepageMenu);