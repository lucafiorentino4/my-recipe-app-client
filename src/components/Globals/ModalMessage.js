import React, {Component} from 'react';

import { Button, Header, Icon, Modal } from 'semantic-ui-react'

class ModalMessage extends Component {

    render() {
        return (
            <Modal size='small' open={this.props.modalOpen} onClose={this.props.closeModalHandler}>
                <Header>
                    <Icon name={(this.props.icon) ? this.props.icon : 'info'} />
                    {this.props.header}
                </Header>
                <Modal.Content>
                    <p>
                        {this.props.message}
                    </p>
                </Modal.Content>
                <Modal.Actions>
                    <Button color='red' onClick={this.props.closeModalHandler}>
                        <Icon name='remove' /> No
                    </Button>
                    <Button color='green'  onClick={this.props.confirmModalHandler}>
                        <Icon name='checkmark' /> Yes
                    </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default ModalMessage;