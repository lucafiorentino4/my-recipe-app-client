import styled from 'styled-components';
import {Feed, Header, Image, Label, Card} from 'semantic-ui-react';


// styled components of semantic ui react
export const StyledHeader = styled(Header)({
    marginTop: "0.3rem !important",
    marginBottom: "0.3rem !important"});
export const StyledImage = styled(Image)({
    marginTop: "0.8rem !important"
});
export const StyledFeedSummary = styled(Feed.Summary)({
    fontSize: "0.8rem !important"
});
export const StyledFeedContent = styled(Feed.Content)({
    margin: ".0em 0 0em 1.14285714em !important",
    display: "flex !important",
    alignItems: "center !important",
    lineHeight: "0.9rem !important"
});
export const StyledFeedLabel = styled(Feed.Label)({
    width: "2.2rem !important"
});
export const StyledLabel = styled(Label)({
    fontSize: "0.9rem !important",
    margin: "0.5rem 0.5rem !important"
});
export const StyledListCardImage = styled(Image)({
    maxHeight:  "150px !important",
    overflow: "hidden !important"
});
export const StyledCardHeader = styled(Card.Header)({
   fontSize: "1rem !important",
   marginBottom: "0.5rem !important"
});

