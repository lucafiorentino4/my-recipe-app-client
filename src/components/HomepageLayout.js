import React, {Component} from 'react';
import {Route} from "react-router-dom";

import Home from "./HomepageContent/HomeContent/Home.js"
import Login from "./HomepageContent/Login.js"
import Register from "./HomepageContent/Register.js"

import NewRecipe from "./Recipes/NewRecipe.js";
import MyRecipes from "./Recipes/MyRecipes.js";
import RecipeView from  "./Recipes/RecipeViewComponents/RecipeView.js"
import EditRecipe from "./Recipes/RecipeViewComponents/EditRecipe.js";
import PublicRecipeView from "./HomepageContent/HomeContent/PublicRecipes/PublicRecipeView/PublicRecipeView.js";

import ResponsiveContainer from "./HomepageContainer/ResponsiveContainer.js";

import PrivateRoute from "../service/PrivateRoute";
import ExploreRecipes from "./Explore/ExploreRecipes";

class HomepageLayout extends Component {
    render() {

        return (
            <ResponsiveContainer>

                {/* Routes */}
                <Route exact path="/" component={Home}/>

                <PrivateRoute path="/publicrecipeview/:recipe_id" component={PublicRecipeView} />

                <Route path="/publicrecipeviewoffline/:recipe_id" component={PublicRecipeView} />

                <Route path="/login" component={Login}/>

                <Route path="/register" component={Register}/>

                <PrivateRoute path="/newrecipe" component={NewRecipe}/>

                <PrivateRoute path="/myrecipes" component={MyRecipes}/>

                <PrivateRoute path="/recipeview/:recipe_id" component={RecipeView} />

                <PrivateRoute path="/editrecipe/:recipe_id" component={EditRecipe} />

                <PrivateRoute path="/explorerecipes" component={ExploreRecipes} />


            </ResponsiveContainer>
        );
    }
}

export default HomepageLayout;
