import React, {Component} from 'react';

import {Responsive, Visibility, Container} from "semantic-ui-react";
import HomepageMenu from "../HomepageMenu/HomepageMenu";

import HeroImage from "../HomepageContent/HomeContent/HeroImage";
import auth from "../../service/auth";
import {withRouter} from "react-router-dom";

class DesktopContainer extends Component {

    state = {};


    render() {

        return (
            <Responsive minWidth={Responsive.onlyTablet.minWidth}>

                <Visibility >

                        <HomepageMenu/>
                        {(!auth.isAuthenticated)
                            ? <HeroImage />
                            : ' '}

                </Visibility>

                <Container className="containerMarginTop">
                    {this.props.children}
                </Container>

            </Responsive>
        );
    }


}

export default withRouter(DesktopContainer);