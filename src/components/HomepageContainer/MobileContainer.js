import React, {Component} from 'react';
import {Sidebar, Responsive, Menu, Icon, Button, Container} from "semantic-ui-react";
import HomepageMenu from "../HomepageMenu/HomepageMenu";
import AuthButton from "../HomepageMenu/AuthButton";
import HeroImage from "../HomepageContent/HomeContent/HeroImage";
import NavBottomMobile from "../HomepageMenu/NavBottomMobile";
import {Link, withRouter} from "react-router-dom";
import Logo from "../HomepageMenu/Logo";
import auth from "../../service/auth";

class MobileContainer extends Component {

    state = {

    };

    componentDidUpdate(prevProps, prevState) {
        if (this.state.sidebarOpened && prevState.sidebarOpened)
            this.setState({sidebarOpened: false});
    }

    closeSidebar = () => {
      if (this.state.sidebarOpened) this.setState({sidebarOpened:false});
    };

    toggleSidebar = () => this.setState({sidebarOpened: !this.state.sidebarOpened});

    render() {
        const {sidebarOpened} = this.state;

        return (
            <Responsive maxWidth={Responsive.onlyMobile.maxWidth}>
                <Sidebar.Pushable >
                    <Sidebar visible={sidebarOpened} onClick={this.closeSidebar}>
                        <HomepageMenu mobile={true} sidebar={true}/>
                        <Button icon basic onClick={this.closeSidebar} className="closeMenu" size="small">
                            <Icon name='close' />
                        </Button>
                    </Sidebar>

                    <Sidebar.Pusher dimmed={sidebarOpened} onClick={this.closeSidebar} className="sideBarPusherStyle" >
                        <Menu borderless secondary pointing className="menuStyleMobileContainer" >
                            <Menu.Item  onClick={this.toggleSidebar} position='left'>
                                <span className="iconStyleNavTop">
                                    <Icon  name='sidebar'/>
                                    <p>Menu</p>
                                </span>
                            </Menu.Item>

                            <Menu.Menu className="logoCentered">
                                <Menu.Item as={Link} to="/" name='home' className="Logo mobile"> <Logo /> </Menu.Item>
                            </Menu.Menu>

                            <AuthButton mobile={true}/>

                        </Menu>

                        {(!auth.isAuthenticated)
                            ? <HeroImage />
                            : ' '}

                        <Container className="containerMarginTop" >
                            {this.props.children}
                        </Container>

                    </Sidebar.Pusher>
                </Sidebar.Pushable>
                <Menu borderless>
                    <NavBottomMobile />
                </Menu>
            </Responsive>
        );
    }
}

export default withRouter(MobileContainer);