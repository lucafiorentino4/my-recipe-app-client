import React from 'react';

import DesktopContainer from "./DesktopContainer";
import MobileContainer from "./MobileContainer";

import { withRouter } from "react-router-dom";

const ResponsiveContainer = withRouter(({children}) => (
    <div>
        <DesktopContainer>{children}</DesktopContainer>
        <MobileContainer>{children}</MobileContainer>
    </div>
));



export default ResponsiveContainer;