import React, {Component} from 'react';

import {Redirect} from 'react-router-dom';

import { Button, Form } from 'semantic-ui-react'

import apiService from "../../service/apiService";

class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {

            redirectToLogin: false,

            user: {
                firstname: "",
                lastname: "",
                nickname: "",
                email: "",
                password: "",
                passwordRepeat: "",
            },

            errors: {
                firstname: null,
                lastname: null,
                nickname: null,
                email: null,
                password: null,
                passwordRepeat: null,
            }
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.submit = this.submit.bind(this);

        this.validate = this.validate.bind(this);
    }

    changeHandler(evt, {name, value}) {
        let user = this.state.user;
        user[name] = value;
        this.setState({user:user});
    }


    //check for errors in register
    validate() {
        let {user, errors} = this.state;
        let isError = false;

        for (let [key, value] of Object.entries(user)) {
            let regex = /^[a-zA-Z0-9.!#$%&'+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)$/;
            if (value === '') {
                isError = true;
                errors[key] = `${key} is empty.`;
            } else {
                errors[key] = null
            }
            if (key === 'email' && regex.test(value) === false){
                isError = true;
                errors['email'] = `this ${key} is not valid.`;
            }
            if (key === 'password' && value.length < 8){
                isError = true;
                errors['password'] = `${key} is too short (min. 8 characters).`;
            }
            if (key === 'password') {
                if(value !== user.passwordRepeat){
                    isError = true;
                    errors['password'] = `The two passwords do not match.`;
                }
            }
        }
        return (isError) ? errors : false;
    }

    submit() {
        // prepare error
        let errors = this.validate();
        // if no errors -> login
        // else render errors
        if(!errors){
            apiService.registerUser(this.state.user).then(() => {
                this.setState({redirectToLogin: true});
            }).catch(err => {
                let errors = this.state.errors;
                errors['email'] = err.data;
                this.setState({errors: errors});
            });
        } else {
            this.setState({errors: errors})
        }
    }

    render() {
        const {user, errors, redirectToLogin} = this.state;
        // redirectToLogin = true than redirect
        if (redirectToLogin) return <Redirect to="/login" />;


        return (
            <Form>

                <Form.Group widths='equal'>
                    <Form.Field>
                        <Form.Input label="Firstname" placeholder='Firstname' name="firstname" value={user.firstname} error={(!!errors.firstname) ? { content: errors.firstname } : null} onChange={this.changeHandler}/>
                    </Form.Field>
                    <Form.Field>
                        <Form.Input label="Lastname" placeholder='Lastname' name="lastname" value={user.lastname} error={(!!errors.lastname) ? { content: errors.lastname } : null} onChange={this.changeHandler}/>
                    </Form.Field>
                    <Form.Field>
                        <Form.Input label="Nickame" placeholder='Nickame' name="nickname" value={user.nickname} error={(!!errors.nickname) ? { content: errors.nickname } : null} onChange={this.changeHandler}/>
                    </Form.Field>
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.Field>
                        <Form.Input label="Email" placeholder='Email' name="email"  value={user.email} error={(!!errors.email) ? { content: errors.email } : null} onChange={this.changeHandler}/>
                    </Form.Field>
                    <Form.Field>
                        <Form.Input type="password" label="Password" placeholder='Password' name="password"  value={user.password} error={(!!errors.password) ? { content: errors.password } : null} onChange={this.changeHandler}/>
                    </Form.Field>
                    <Form.Field>
                        <Form.Input type="password" label="Password repeat" placeholder='Password repeat' name="passwordRepeat" value={user.passwordRepeat} error={(!!errors.passwordRepeat) ? { content: errors.passwordRepeat } : null} onChange={this.changeHandler}/>
                    </Form.Field>
                </Form.Group>
                <Button type='submit' onClick={this.submit}>Submit</Button>
            </Form>
        );
    }
}

export default Register;