import React, {Component} from 'react';
import { Button, Form, Message, Grid, Segment, Responsive, Divider} from "semantic-ui-react";
import {Link, Redirect} from "react-router-dom";
import apiService from "../../service/apiService";
//---------------------------------------------------------------------------------------
import auth from '../../service/auth';
//---------------------------------------------------------------------------------------

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToReferrer: false,
            values: {
                email: "",
                password: "",
            },
            errors: {
                email: null,
                password: null,
                login: null,
            }
        };
        this.changeHandler = this.changeHandler.bind(this);
        this.submit = this.submit.bind(this);
        this.validate = this.validate.bind(this);
    }

    // set state
    changeHandler(evt, {name, value}) {
        let values = this.state.values;
        let errors = this.state.errors;
        values[name] = value;
        errors[name] = null;
        this.setState({values:values, errors:errors});
    }

    // check for errors
    validate() {
        let values = this.state.values;
        let errors = this.state.errors;
        let isError = false;
        for (let [key, value] of Object.entries(values)) {
            if (value === '') {
                isError = true;
                errors[key] = `${key} is empty.`;
            } else {
                errors[key] = null
            }
        }
        return (isError) ? errors : false;
    }

    // do login
    submit() {
        // prepare error
        let errors = this.validate();
        // if no errors -> login
        // else render errors
        if (!errors) {
            let user = this.state.values;
            apiService.loginUser(user).then(data => {
                //if no errors than login
                auth.authenticate(data.user, data.token, () => {
                    this.setState({redirectToReferrer: true});
                });
            }).catch(error => {
                let errors = this.state.errors;
                errors['login'] = error.data;
                this.setState({errors: errors});
            })
        } else {
            this.setState({errors:errors});
        }
    }

    render() {
        const {from} = this.props.location.state || {from: {pathname: '/'}};
        const {redirectToReferrer} = this.state;
        if (redirectToReferrer) return <Redirect to={from}/>;

        const {errors} = this.state;
        let errorList = [];
        // eslint-disable-next-line  no-unused-vars
        for (let [key, value] of Object.entries(errors)) {
            if (value) errorList.push(value);
        }

        return (
            <Segment placeholder>
                <Grid columns={2} relaxed='very' stackable>
                    <Grid.Column>
                        <Form error={(!!errors.login || !!errors.email || !!errors.password)}>
                            <Message error
                                     header='Something went wrong!'
                                     list={errorList}
                            />
                            <Form.Input
                                icon='user'
                                iconPosition='left'
                                name="email"
                                label="Email"
                                placeholder='Email'
                                error={(!!errors.email) ? { content: errors.email } : null}
                                onChange={this.changeHandler}
                            />
                            <Form.Input
                                icon='lock'
                                iconPosition='left'
                                label="Password"
                                placeholder='Password'
                                name="password"
                                error={(!!errors.password) ? { content: errors.password } : null}
                                onChange={this.changeHandler}
                                type="password"
                            />

                            <Button className="CIButtercup" type='submit' onClick={this.submit}>Submit</Button>
                        </Form>
                    </Grid.Column>

                    <Responsive  as={Grid.Column} {...Responsive.onlyMobile}>
                        {/* Mobile Divider */}
                        <Divider horizontal>Or</Divider>
                    </Responsive>

                    <Grid.Column verticalAlign='middle'>
                        <Button className="CIBurntSienna" as={Link} to="/register" name='register' content='Sign up' icon='signup' size='big' />
                    </Grid.Column>
                </Grid>

                {/* Desktop Divider */}
                <Responsive as={Divider} minWidth={Responsive.onlyTablet.minWidth} vertical >Or</Responsive>

            </Segment>
        );
    }
}

export default Login;