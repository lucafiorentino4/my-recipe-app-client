import React, {Component} from 'react';
import {Card, Grid, Icon, Rating} from "semantic-ui-react";
import {config} from "../../../../Config";
import {Link} from "react-router-dom";
import {StyledListCardImage, StyledCardHeader} from "../../../Globals/StyledReactComponents/StyledReactComponents.js"
import Feed from "semantic-ui-react/dist/commonjs/views/Feed";
import {
    StyledFeedContent,
    StyledFeedLabel,
    StyledFeedSummary
} from "../../../Globals/StyledReactComponents/StyledReactComponents";
//-------------------------------------

class RecipeItem extends Component {
    // calculate Time
    calculateTimeToString(time) {
        let hours = Math.trunc(time / 60);
        let minutes = (time % 60);
        return ((hours === 0) ? "" : hours + ':')  + minutes + ((hours > 0) ? " h." : " min.");
    }

    render() {
        let recipe = this.props.recipeItem;

        // addition of Cooking Time Preparation and waiting Time
        let addedTimes = recipe.preparationTime + recipe.waitingTime + recipe.cookingTime;

        return (
            <Grid.Column mobile={8} tablet={4} computer={4} >
                <Card centered as={Link} to={"/recipeview/"+recipe.id}>
                    <StyledListCardImage wrapped ui={false}>
                        <img src={config.webServer + recipe.image_url} alt={recipe.name}/>
                        <label className="timeStyleCard"><Icon name='clock' />{this.calculateTimeToString(addedTimes)}</label>
                    </StyledListCardImage>
                    <Card.Content>
                        <Card.Header>
                            <label className="likeHeart redCI" >
                                <Icon name="heart" className="likeIcon"/>
                                <span className="likeCount">{recipe.like_count}</span>
                            </label>
                        </Card.Header>
                        <StyledCardHeader>{recipe.name}</StyledCardHeader>
                        <Card.Meta>
                            <Rating disabled maxRating={5} rating={recipe.difficulty_id} icon='star' size='large' />
                            <span className='date'>{recipe.difficulty_id}</span>
                        </Card.Meta>
                        <Card.Meta>
                            <span>{recipe.portions} {(recipe.portions > 1) ? (recipe.portionUnit+"s") : recipe.portionUnit}</span>
                         </Card.Meta>
                    </Card.Content>
                    <Card.Content extra>
                        <Feed>
                            <Feed.Event>
                                <StyledFeedLabel image={require('../../../../assets/molly.png')} />
                                <StyledFeedContent>
                                    <StyledFeedSummary>
                                        {recipe.user_firstname + " " + recipe.user_lastname}
                                    </StyledFeedSummary>
                                </StyledFeedContent>
                            </Feed.Event>
                        </Feed>
                    </Card.Content>
                </Card>
            </Grid.Column>
        );
    }
}

export default RecipeItem;