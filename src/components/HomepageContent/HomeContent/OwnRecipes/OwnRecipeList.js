import React, {Component} from 'react';
import {Grid} from "semantic-ui-react";
import RecipeItem from "./RecipeItem.js";

class OwnRecipeList extends Component {
    render() {
        let OwnRecipeList = this.props.allRecipes.map((item) => {
            return <RecipeItem key={item.id} recipeItem={item} index={item.id}/>;
        });

        return (
            <Grid>
                <Grid.Column width={13} as="h3">My own recipes</Grid.Column>
                {OwnRecipeList}
            </Grid>
        );
    }
}

export default OwnRecipeList;