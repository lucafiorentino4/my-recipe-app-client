import React, {Component} from 'react';
import {Header} from 'semantic-ui-react';
import apiService from "../../../service/apiService.js"
import auth from "../../../service/auth.js";
import OwnRecipeList from "./OwnRecipes/OwnRecipeList.js";
import PublicRecipeList from "./PublicRecipes/PublicRecipeList.js";


class Home extends Component {

    constructor(props){
        super(props);

        this.state = {
            allRecipes: [],
            allPublicRecipes: []
        };
        this.getDataFromServer = this.getDataFromServer.bind(this);
    }

    componentDidMount() {
       this.getDataFromServer();
    }



    getDataFromServer() {
    if (auth.isAuthenticated){
            let user_id = auth.user.id;
            // get 4 Recipes that the user have create
            apiService.getFirstFourRecipesForUser(user_id).then(data => {
                this.setState({allRecipes: data});
            });
            // get all Public recipes for the user that is Login
            apiService.getAllPublicRecipes(user_id).then(data => {
                this.setState({allPublicRecipes: data});
            });
        } else {
            // get only 10 Public recipes for the user that is Logout
            apiService.getTenPublicRecipes().then(data => {
                this.setState({allPublicRecipes: data});
            });
        }
    }


    render() {

        if(auth.isAuthenticated){
            return (
                <div className="HomepageMargin" style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                    <div>
                        <OwnRecipeList allRecipes={this.state.allRecipes} />
                        <PublicRecipeList allPublicRecipes={this.state.allPublicRecipes} changeLikeHandler={this.changeLike}/>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="HomepageMargin" style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                    <div style={{textAlign:"center"}}>
                        <Header as='h2' inverted style={{fontSize: '1.7em', fontWeight: 'normal', marginTop: 0, paddingBottom: '1em'}}>
                            Login to create and publish your own recipe
                        </Header>
                        <PublicRecipeList allPublicRecipes={this.state.allPublicRecipes} />
                    </div>
                </div>

            );
        }

    }
}

export default Home;