import React, {Component} from 'react';
import {Grid} from "semantic-ui-react";
import {withRouter} from "react-router-dom";
import PublicRecipeItem from "./PublicRecipeItem.js";

class PublicRecipeList extends Component {
    render() {
        let PublicRecipeList = this.props.allPublicRecipes.map((item) => {
            return <PublicRecipeItem key={item.id} recipeItem={item} index={item.id} changeLikeHandler={this.props.changeLikeHandler}/>;
        });

        return (
            <Grid style={{textAlign:"left"}}>
                <Grid.Column width={16} as="h3">Recipes</Grid.Column>
                {PublicRecipeList}
            </Grid>
        );
    }
}

export default  withRouter(PublicRecipeList);