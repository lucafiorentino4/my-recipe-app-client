import React, {Component} from 'react';
import {Button, Grid} from "semantic-ui-react";
import apiService from "../../../../../service/apiService";
import PublicRecipeInfos from "../../PublicRecipes/PublicRecipeView/PublicRecipeInfos.js";
import auth from "../../../../../service/auth";


class PublicRecipeView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            recipeData: null
        };

        this.getDataFromServer = this.getDataFromServer.bind(this);
        this.changeLike = this.changeLike.bind(this);
    }


    componentDidMount() {
        this.getDataFromServer();
    }


    getDataFromServer(){
        const { recipe_id } = this.props.match.params;

        // Get data
        if(auth.isAuthenticated){
            let user_id = auth.user.id;
            // if user login, get recipe with user_id
            apiService.getRecipe(recipe_id, user_id).then(data => {
                this.setState({recipeData: data});
            });
        } else {
            // if user logout, get recipe without user_id
            apiService.getRecipeFromOffline(recipe_id).then(data => {
                this.setState({recipeData: data});
            });
        }
    }

    // change like handler
    changeLike(recipe_id, like_recipe) {
        let user_id = auth.user.id;
        apiService.likeRecipe(user_id, recipe_id, like_recipe).then(() => {
            this.getDataFromServer();
        });
    }


    render(){
        if(!this.state.recipeData) return null;

        let recipeData = this.state.recipeData;


        return(
            <Grid className="extra paddings">
                <Button.Group labeled icon floated="left" className="button margin">
                    <Button onClick={this.props.history.goBack} className="lightGrey" icon='arrow alternate circle left' content='Back' />
                </Button.Group>
                <PublicRecipeInfos recipeItem={recipeData} deleteRecipeHandler={this.deleteRecipe} changeLikeHandler={this.changeLike}/>
            </Grid>
        )
    }
}
export default PublicRecipeView;