import React, {Component} from 'react';
import {Icon, List, Rating} from "semantic-ui-react";
import {StyledLabel} from "../../../../Globals/StyledReactComponents/StyledReactComponents";
import auth from "../../../../../service/auth";

class PublicRecipeInfoBlock extends Component {

    render() {

        let recipeItem = this.props.recipeItem;

        let recipeOccasion = this.props.recipeItem.occasion;

        // prepare occasion labels
        let occasionLabel = recipeOccasion.map((item) => {
            return <StyledLabel key={item.id} size="large" className="CIButtercup" horizontal>{ item.occasion_name }</StyledLabel>
        });

        return (
            <div>
                <div>
                    <List divided>
                        <List.Item>
                            <StyledLabel size="large" className="CIButtercup" horizontal>{ recipeItem.cat_name }</StyledLabel>
                            <StyledLabel size="large" className="CIButtercup" horizontal>{ recipeItem.rt_name }</StyledLabel>
                            <StyledLabel size="large" className="CIButtercup" horizontal>{ recipeItem.kl_name }</StyledLabel>
                            {occasionLabel}
                        </List.Item>
                    </List>
                    <label className={(recipeItem.like_recipe) ? "likeHeart redCI" : "likeHeart rosa"} >
                        <Icon name={(recipeItem.like_recipe) ? "heart" : "heart outline"} className="likeIcon" onClick={(auth.isAuthenticated) ? (() => {this.props.changeLikeHandler(recipeItem.id, !recipeItem.like_recipe)}) : null} />
                        <span className={(recipeItem.like_recipe) ? "likeCount redCI" : "likeCount rosa"}>{recipeItem.like_count}</span>
                        <p className="likeText">{(recipeItem.like_count > 1) ? "Persons" : "Person"} Like it</p>
                    </label>
                    <div className="RecipeInfosLabel">
                        <div className="RecipeInfosLabelContainer">
                            <label>Difficulty:</label>
                            <div className="RecipeInfoDiv">
                                <Rating disabled maxRating={5} rating={recipeItem.difficulty_id} icon='star' size='huge' />
                                <label >{(recipeItem.difficulty_text !== null) ? (" | " + recipeItem.difficulty_text) : "No difficulty added" }</label>
                            </div>
                        </div>
                        <div className="RecipeInfosLabelContainer">
                            <label>{recipeItem.portionUnit}:</label>
                            <div className="RecipeInfoDiv">
                                <Icon name='food' size='large'/> x {recipeItem.portions}
                            </div>
                        </div>
                    </div>
                    <div className="recipeTime">
                        {(recipeItem.preparationTime === 0) ? " " : (
                            <label>
                                <span>Preparation Time:</span>
                                <span><Icon name='clock' />{this.props.calculateTimeToString(recipeItem.preparationTime)}</span>
                            </label>)}
                        {(recipeItem.cookingTime === 0) ? " " : (
                            <label>
                                <span>Cocking Time:</span>
                                <span><Icon name='clock' />{this.props.calculateTimeToString(recipeItem.cookingTime)}</span>
                            </label>)}
                        {(recipeItem.waitingTime === 0) ? " " : (
                            <label>
                                <span>Rest Time:</span>
                                <span><Icon name='clock' />{this.props.calculateTimeToString(recipeItem.waitingTime)}</span>
                            </label>)}
                    </div>
                </div>
            </div>
        );
    }
}

export default PublicRecipeInfoBlock;