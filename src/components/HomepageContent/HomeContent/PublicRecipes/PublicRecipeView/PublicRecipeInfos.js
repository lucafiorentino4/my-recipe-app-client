import React, {Component} from 'react';
import {Grid, Image} from "semantic-ui-react";
import { config } from "../../../../../Config"
import Item from "semantic-ui-react/dist/commonjs/views/Item";
import Responsive from "semantic-ui-react/dist/commonjs/addons/Responsive";
import PublicRecipeInfoBlock from "./PublicRecipeInfoBlock";

//-------------------------------------

class PublicRecipeInfos extends Component {

        constructor(props) {
            super(props);

            this.state = {
            }
        }
    //------------------------------------------------------------------------------------------------------------------
        // prepare Time
        calculateTimeToString(time) {
            let hours = Math.trunc(time / 60);
            let minutes = (time % 60);
            return ((hours === 0) ? "" : hours + ':')  + minutes + ((hours > 0) ? " h." : " min.");
        }

    render() {
        if(!this.props.recipeItem) return null;
        // get state
        let recipeItem = this.props.recipeItem;


        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column mobile={16} tablet={6} computer={6} >
                        <Item.Group>
                            <Item className="styleItem">
                                <Item.Image size='tiny' src={require('../../../../../assets/molly.png')} className="styledItemImage" />
                                <Item.Content verticalAlign='middle' className="styledItemHeader">
                                    <Item.Header>
                                        {recipeItem.user_firstname+ ", " + recipeItem.user_lastname}
                                    </Item.Header>
                                </Item.Content>
                            </Item>
                        </Item.Group>
                        <Responsive minWidth={Responsive.onlyTablet.minWidth}>
                            <PublicRecipeInfoBlock recipeItem={recipeItem} calculateTimeToString={this.calculateTimeToString} />
                        </Responsive>
                    </Grid.Column>
                    <Grid.Column mobile={16} tablet={10} computer={10} >
                        <div className="RecipeViewPicContainerStyle">
                            <div className="RecipeViewLabelOnPicStyle">{recipeItem.name}</div>
                            <Image className="RecipeViewPicStyle" src={config.webServer + recipeItem.image_url} />
                        </div>
                    </Grid.Column>
                </Grid.Row>
                <Responsive {...Responsive.onlyMobile}>
                    <PublicRecipeInfoBlock recipeItem={recipeItem} calculateTimeToString={this.calculateTimeToString} />
                </Responsive>
                <Grid.Row>
                    <Grid.Column mobile={16} tablet={12} computer={12}>
                        <div className="RecipeInfosLabel">
                            <label>Recipe Description:</label>
                            {/*recipe.description */}
                        </div>
                        <div className="RecipeDescription">
                            <div dangerouslySetInnerHTML={{__html: recipeItem.description}} className="descriptionColor" />
                        </div>
                    </Grid.Column>
                </Grid.Row>
            </Grid>

        );
    }
}

export default PublicRecipeInfos;