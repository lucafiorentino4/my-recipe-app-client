import React, {Component} from 'react';
import {config} from "../../../../Config";
import {Card, Grid, Icon,  Rating} from "semantic-ui-react";
import {StyledListCardImage, StyledFeedSummary, StyledFeedLabel, StyledFeedContent, StyledCardHeader} from "../../../Globals/StyledReactComponents/StyledReactComponents.js"
import Feed from "semantic-ui-react/dist/commonjs/views/Feed";
import { Link } from "react-router-dom";
import auth from "../../../../service/auth";

//-------------------------------------

class PublicRecipeItem extends Component {
    // Prepare Time
    calculateTimeToString(time) {
        let hours = Math.trunc(time / 60);
        let minutes = (time % 60);
        return ((hours === 0) ? "" : hours + ':')  + minutes + ((hours > 0) ? " h." : " min.");
    }

    render() {
        let recipe = this.props.recipeItem;

        let addedTimes = recipe.preparationTime + recipe.waitingTime + recipe.cookingTime;

        return (
            <Grid.Column mobile={8} tablet={4} computer={4} >
                <Card centered  as={Link} to={(auth.isAuthenticated) ? ("/publicrecipeview/"+recipe.id) : ("/publicrecipeviewoffline/"+recipe.id)} >
                    <StyledListCardImage >
                        <img src={config.webServer + recipe.image_url} alt={recipe.name}/>
                        <label className="timeStyleCard"><Icon name='clock' />{this.calculateTimeToString(addedTimes)}</label>
                    </StyledListCardImage>
                    <Card.Content style={{position: "relative"}}>
                        <Card.Header>
                                <label className="likeHeart redCI" >
                                     <Icon name="heart" className="likeIcon"/>
                                     <span className="likeCount">{recipe.like_count}</span>
                                </label>
                        </Card.Header>
                        <Card.Header>
                            <StyledCardHeader>
                                {recipe.name}
                            </StyledCardHeader>
                        </Card.Header>
                        <Card.Meta>
                            <Rating disabled maxRating={5} rating={recipe.difficulty_id} icon='star' size='large' />
                            <span className='date'>{recipe.difficulty_id}</span>
                        </Card.Meta>
                        <Card.Meta>
                            <span>{recipe.portions} {(recipe.portions > 1) ? (recipe.portionUnit+"s") : recipe.portionUnit}</span>
                        </Card.Meta>
                    </Card.Content>
                    <Card.Content >
                        <Feed>
                            <Feed.Event>
                                <StyledFeedLabel image={require('../../../../assets/molly.png')} />
                                <StyledFeedContent>
                                    <StyledFeedSummary>
                                       {recipe.user_firstname + " " + recipe.user_lastname}
                                    </StyledFeedSummary>
                                </StyledFeedContent>
                            </Feed.Event>
                        </Feed>
                    </Card.Content>
                </Card>
            </Grid.Column>
        );
    }
}

export default PublicRecipeItem;