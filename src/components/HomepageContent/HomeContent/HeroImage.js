import React, {Component} from 'react';

import { Link, withRouter } from "react-router-dom";

import {Container, Header, Button, Icon} from "semantic-ui-react";

class HeroImage extends Component {
    render() {

        let location = this.props.location;

        // disable HeroImage on all sites except home
        switch (location.pathname) {
            case '/':
                break;
            default:
                return null;
        }

        return (
            <div className="HomepageMargin" style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <Container style={{textAlign:"center"}}>
                    <Header as='h1' inverted style={{fontSize: '4em', fontWeight: 'bold', marginTop: 0}} >
                        Your Recipe Book
                    </Header>
                    <Header as='h2' inverted style={{fontSize: '1.7em', fontWeight: 'normal', marginTop: 0, paddingBottom: '1em'}}>
                       Create your digital recipe book
                    </Header>
                    <Button as={Link} to="/login" className="CIApricot" size="huge">
                        Discover new recipes
                        <Icon name="right arrow"/>
                    </Button>
                </Container>
            </div>
        );
    }
}

export default withRouter(HeroImage);