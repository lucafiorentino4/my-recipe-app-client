import React, {Component} from 'react';
import apiService from "../../service/apiService";
import  {Grid} from "semantic-ui-react";
import Search from "semantic-ui-react/dist/commonjs/modules/Search";
import {StyledHeader, StyledImage} from "../Globals/StyledReactComponents/StyledReactComponents";
import PublicRecipeList from "../HomepageContent/HomeContent/PublicRecipes/PublicRecipeList";
import auth from "../../service/auth";

class ExploreRecipes extends Component {

    constructor(props) {
        super(props);
        //let user_id = auth.user.id;
        this.state = {
            /* Start Search */
            isLoading: false,
            searchModeOn: true,
            searchValue: "",
            searchResult: null,
            redirectToHome: false,
        };

        this.searchChangeHandler = this.searchChangeHandler.bind(this);
        this.changeLike = this.changeLike.bind(this);
        this.getDataFromServer = this.getDataFromServer.bind(this);
    }

    componentDidMount() {
        this.getDataFromServer();
    }

    getDataFromServer(){
        let user_id = auth.user.id;
        // get all Public recipes for the user that is Login
        apiService.getAllPublicRecipes(user_id).then(data => {
            this.setState({allPublicRecipes: data});
        });
    }

    // set the SearchValue
    searchChangeHandler(evt, {value}){
        let user_id = auth.user.id;
        this.setState({searchValue: value});
        let searchValue = this.state.searchValue;

        // if Search Value has more than two Characters than start to search
        if (searchValue.length === 2){
            apiService.getAllPublicRecipesWithSearch(user_id, searchValue).then(data => {
                this.setState({searchResult: data});
            })
        }
    }


    // Change Like when open Recipe
    changeLike(recipe_id, like_recipe){
        let user_id = auth.user.id;

        apiService.likeRecipe(recipe_id, user_id, like_recipe).then(() => {
            let searchValue = this.state.searchValue;
            apiService.getAllPublicRecipesWithSearch(user_id, searchValue).then(data => {
                this.setState({searchResult: data});
            })
        });
    }


     render() {
         let searchResult = this.state.searchResult;
         let searchValue =  this.state.searchValue;
         if(!this.state.allPublicRecipes) return null;
         let allPublicRecipes = this.state.allPublicRecipes;

         let searchBar =
             <Grid>
                 <Grid.Row>
                     <Grid.Column mobile={16} tablet={16} computer={16} textAlign="center">
                         <Search value={searchValue} size="large" onSearchChange={this.searchChangeHandler} showNoResults={false} />
                     </Grid.Column>
                 </Grid.Row>
             </Grid>;


         if (this.state.searchResult){
             return (
                    <div>
                        {searchBar}
                        <PublicRecipeList allPublicRecipes={searchResult} changeLikeHandler={this.changeLike} />
                    </div> );
            } else {
               return(
                   <div>
                     {searchBar}
                       {(this.state.searchResult === false)
                           ? (
                               <Grid>
                                   <Grid.Row>
                                       <Grid.Column textAlign="center" >
                                           <StyledHeader as="h2">Oh Snap!</StyledHeader>
                                           <StyledHeader as="h3">No recipes found, try again!</StyledHeader>
                                           <StyledImage centered size="medium" src={require("../../assets/monster.svg")}/>
                                       </Grid.Column>
                                   </Grid.Row>
                               </Grid>
                           ) : (
                               <Grid >
                                   <Grid.Row centered>
                                       <Grid.Column textAlign="center" >
                                           <StyledHeader as="h2">
                                               Searching...
                                           </StyledHeader>
                                           <PublicRecipeList allPublicRecipes={allPublicRecipes} changeLikeHandler={this.changeLike} />
                                       </Grid.Column>
                                   </Grid.Row>
                               </Grid>
                           )}
                   </div>);
             }
          }
}

export default ExploreRecipes;