import React, {Component} from 'react';

import {BrowserRouter as Router} from 'react-router-dom';

//------------------------------

import HomepageLayout from "./components/HomepageLayout";

class Application extends Component {
    render() {

        return (
            <Router>

                <HomepageLayout />

            </Router>
        );
    }
}

export default Application;